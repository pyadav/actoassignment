package com.acto.assignment.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.acto.assignment.database.UserEntity


@Dao
interface UserEntityDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUser(user: UserEntity)

    @Query("select * from user")
    fun getUsersList():List<UserEntity>
}