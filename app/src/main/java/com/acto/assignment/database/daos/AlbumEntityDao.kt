package com.acto.assignment.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.acto.assignment.albums.model.Album


@Dao
interface AlbumEntityDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAlbum(user:Album)

    @Query("select * from album")
    fun getAlbumsList():List<Album>
}