package com.acto.assignment.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.acto.assignment.albums.model.Album
import com.acto.assignment.photos.model.Photo


@Dao
interface PhotoEntityDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPhoto(photo:Photo)

    @Query("select * from photo")
    fun getPhotosList():List<Photo>
}