package com.acto.assignment.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey


@Entity(tableName = "user")
 class UserEntity (

    @ColumnInfo(name = "userName") val userName: String,

    @ColumnInfo(name = "phoneNumber") var phoneNumber:String
   )
{
    @PrimaryKey(autoGenerate = true)
    var userid: Int = 0

}