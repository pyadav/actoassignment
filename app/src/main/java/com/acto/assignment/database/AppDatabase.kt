package com.acto.assignment.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.acto.assignment.albums.model.Album
import com.acto.assignment.database.daos.AlbumEntityDao
import com.acto.assignment.database.daos.PhotoEntityDao
import com.acto.assignment.database.daos.UserEntityDao
import com.acto.assignment.photos.model.Photo

@Database(entities = [UserEntity::class,Album::class,Photo::class],version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getUserDao(): UserEntityDao
    abstract fun getAlbumDao(): AlbumEntityDao
    abstract fun getPhotoDao(): PhotoEntityDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: AppDatabase? = null

       public fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "AppDatabase"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}

