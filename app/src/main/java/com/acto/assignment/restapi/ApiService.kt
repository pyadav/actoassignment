package com.acto.assignment.restapi

import com.acto.assignment.albums.model.Album
import com.acto.assignment.photos.model.Photo
import com.acto.assignment.users.model.User
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("users")
    public fun getUsers():Observable<List<User>>

    @GET("albums")
    public fun getAlbums():Observable<List<Album>>

    @GET("photos")
    public fun getPhotos():Observable<List<Photo>>

}