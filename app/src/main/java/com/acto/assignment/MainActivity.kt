package com.acto.assignment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.acto.assignment.databinding.ActivityMainBinding
import com.acto.assignment.albums.view.ui.AlbumsListActivity
import com.acto.assignment.photos.view.ui.PhotosListActivity
import com.acto.assignment.users.view.ui.UsersListActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.showUsers.setOnClickListener(this)
        binding.showPhotos.setOnClickListener(this)
        binding.showAlbums.setOnClickListener(this)

    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.show_users -> {
                showUsers()
            }
            R.id.show_albums ->{
                showAlbums()
            }
            R.id.show_photos -> {
                showPhotos()
            }
            else -> {

            }
        }

    }

    fun showUsers() {

        intent = Intent(this, UsersListActivity::class.java)
        startActivity(intent)
    }

    fun showAlbums() {

        intent = Intent(this, AlbumsListActivity::class.java)
        startActivity(intent)
    }

    fun showPhotos() {

        intent = Intent(this, PhotosListActivity::class.java)
        startActivity(intent)
    }
}
