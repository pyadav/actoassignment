package com.acto.assignment.base

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.acto.assignment.albums.viewmodel.AlbumsListActivityViewModel
import com.acto.assignment.factory.Injectable
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

open class BaseActivity : AppCompatActivity(), HasActivityInjector, Injectable {

    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    lateinit var albusListActivityViewModel: AlbumsListActivityViewModel

    @Inject
    lateinit var appfactory: ViewModelProvider.Factory

}