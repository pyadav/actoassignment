package com.acto.assignment.factory;

import androidx.lifecycle.ViewModel;
import dagger.MapKey;
import kotlin.annotation.AnnotationRetention;
import kotlin.annotation.AnnotationTarget;

import java.lang.annotation.*;


@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@MapKey
public @interface ViewKey {
    Class<? extends ViewModel> value();

}