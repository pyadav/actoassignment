package com.acto.assignment.di.module

import android.app.Application
import com.acto.assignment.database.AppDatabase
import com.acto.assignment.database.daos.AlbumEntityDao
import com.acto.assignment.database.daos.PhotoEntityDao
import com.acto.assignment.database.daos.UserEntityDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class,RepositoryModule::class,NetworkModule::class])
public class AppModule {


    @Singleton
    @Provides
    fun provideDatabase(app: Application): AppDatabase {
        var appDatabase =  AppDatabase.getDatabase(app)
        return appDatabase;
    }

    @Singleton
    @Provides
    fun provideUserEntityDao(db: AppDatabase): UserEntityDao {
        return db.getUserDao()
    }
    @Singleton
    @Provides
    fun provideAlbumEntityDao(db: AppDatabase): AlbumEntityDao {
        return db.getAlbumDao()
    }

    @Singleton
    @Provides
    fun providePhotoEntityDao(db: AppDatabase): PhotoEntityDao {
        return db.getPhotoDao()
    }

}