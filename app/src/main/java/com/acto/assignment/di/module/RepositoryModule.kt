package com.acto.assignment.di.module

import com.acto.assignment.albums.repository.AlbumListRepository
import com.acto.assignment.users.repository.UsersListRepository
import com.acto.assignment.database.daos.AlbumEntityDao
import com.acto.assignment.database.daos.PhotoEntityDao
import com.acto.assignment.database.daos.UserEntityDao
import com.acto.assignment.users.repository.PhotosListRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RepositoryModule {

    @Provides
    fun providesUsersListRepository(retrofit: Retrofit,user: UserEntityDao): UsersListRepository{

        return UsersListRepository(retrofit,user)
    }


    @Provides
    fun providesAlbumsListRepository(retrofit: Retrofit,album: AlbumEntityDao): AlbumListRepository {

        return AlbumListRepository(retrofit,album)
    }

    @Provides
    fun providesPhotosListRepository(retrofit: Retrofit,photo: PhotoEntityDao): PhotosListRepository {

        return PhotosListRepository(retrofit,photo)
    }

}