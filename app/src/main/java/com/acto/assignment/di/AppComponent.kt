package com.acto.assignment.di

import com.acto.assignment.AppApplication
import com.acto.assignment.di.module.AppModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,AndroidInjectionModule::class, AndroidSupportInjectionModule::class])
public interface AppComponent {

     fun inject(app: AppApplication)
}