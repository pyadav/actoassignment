package com.acto.assignment.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.acto.assignment.factory.AppViewModelProvider
import com.acto.assignment.albums.viewmodel.AlbumsListActivityViewModel
import com.acto.assignment.users.viewmodele.PhotosListActivityViewModel
import com.acto.assignment.users.viewmodele.UsersListActivityViewModel
import com.example.kotlinapplication.factory.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(UsersListActivityViewModel::class)
    public abstract fun bindUsersActivityViewModel(usersListActivityViewModel: UsersListActivityViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AlbumsListActivityViewModel::class)
    public abstract fun bindAlbumsListActivityViewModel(albumsListActivityViewModel: AlbumsListActivityViewModel) : ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(PhotosListActivityViewModel::class)
    public abstract fun bindPhotosListActivityViewModel(photosListActivityViewModel: PhotosListActivityViewModel) : ViewModel



    @Binds
    public abstract fun bindViewModelFactory(factory: AppViewModelProvider): ViewModelProvider.Factory

/*
* inject the appication with @Inject constructur
* bind the view model in viewmodel Module
* add the activity in activity builder
* the the repository in repository module with required consructore parameters
*
*
*
* */
}