package com.acto.assignment.di.module

import com.acto.assignment.albums.view.ui.AlbumsListActivity
import com.acto.assignment.photos.view.ui.PhotosListActivity
import com.acto.assignment.users.view.ui.UsersListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityContributorsModule {


    @ContributesAndroidInjector
    public abstract fun usersListActivity(): UsersListActivity

    @ContributesAndroidInjector
    public abstract fun albumListActivity(): AlbumsListActivity

    @ContributesAndroidInjector
    public abstract fun photosListActivity(): PhotosListActivity


}