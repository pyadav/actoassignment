package com.acto.assignment.di;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.acto.assignment.AppApplication;
import com.acto.assignment.AppApplication;
import com.acto.assignment.factory.Injectable;
import dagger.android.AndroidInjection;
import dagger.android.HasActivityInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

public class AppInject {
    private AppInject(){

    }
    public static void init(AppApplication appApplication){
     DaggerMyAppComponent.builder()
                .application(appApplication)
                .build()
                .inject(appApplication);

        appApplication.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                handleActivities(activity);
                Log.v("TAG","class name "+activity.getLocalClassName());
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    private static void handleActivities(Activity activity) {
        if (activity instanceof HasSupportFragmentInjector){
            AndroidInjection.inject(activity);
        }

        if (activity instanceof HasActivityInjector){
            AndroidInjection.inject(activity);
        }

        if (activity instanceof FragmentActivity){
            ((FragmentActivity)activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(new FragmentManager.FragmentLifecycleCallbacks() {
                @Override
                public void onFragmentPreCreated(@NonNull FragmentManager fm, @NonNull Fragment f, @Nullable Bundle savedInstanceState) {
                    super.onFragmentPreCreated(fm, f, savedInstanceState);
                    if (f instanceof Injectable){
                        AndroidSupportInjection.inject(f);
                    }
                }
            },true);


        }

    }

}
