package com.acto.assignment.di;


import android.app.Application;

import com.acto.assignment.AppApplication;
import com.acto.assignment.di.module.ActivityContributorsModule;
import com.acto.assignment.di.module.AppModule;
import com.acto.assignment.di.module.RepositoryModule;
import com.acto.assignment.di.module.ViewModelModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

import javax.inject.Singleton;


@Singleton
@Component(
        modules = {
                AppModule.class,
                ViewModelModule.class,
                RepositoryModule.class,
                AndroidSupportInjectionModule.class,
                AndroidInjectionModule.class,
                ActivityContributorsModule.class

        }

        )
public interface MyAppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance Builder application(Application application);

        MyAppComponent build();

    }

   public void inject(AppApplication app);

}