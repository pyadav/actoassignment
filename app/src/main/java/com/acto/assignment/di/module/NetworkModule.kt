package com.acto.assignment.di.module

import android.util.Log
import com.acto.assignment.util.AppConstants
import com.acto.assignment.util.UnsafeOkHttpClient
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    var interceptor: CustomInterceptor = CustomInterceptor();

    @Singleton
    @Provides
    fun providesRetrofit(): Retrofit {
        var retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(AppConstants.BASE_URL)

            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();
        return retrofit

    }


    class  CustomInterceptor: Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {

            var request : Request = chain.request()

            Log.v("TAG","request: "+request)
            var response: Response = chain.proceed(chain.request());

            Log.v("TAG","response: "+response)

            // How to add extra headers?
            Log.v("TAG","Body "+response.body())

            return response;
        }

    }

    fun client():OkHttpClient {
        val client = OkHttpClient().newBuilder()
            .addInterceptor(interceptor)
            .build()

        return client
    }
}