package com.acto.assignment.users.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.acto.assignment.R
import com.acto.assignment.databinding.ActivityUsersListBinding
import com.acto.assignment.users.viewmodele.UsersListActivityViewModel
import com.acto.assignment.database.UserEntity
import androidx.lifecycle.Observer
import com.acto.assignment.base.BaseActivity
import com.acto.assignment.users.view.adapter.UsersListAdapter
import com.acto.assignment.util.AppConstants
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_users_list.*
import javax.inject.Inject

class UsersListActivity : BaseActivity(), UsersListAdapter.ItemClckListner {

    lateinit var binding : ActivityUsersListBinding
    lateinit var usersListActivityViewModel: UsersListActivityViewModel
    lateinit var usersListAdapter: UsersListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_users_list)
        AndroidInjection.inject(this)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_users_list)
        binding.lifecycleOwner = this;
        //set actionbar title
        var actionbar = getSupportActionBar()
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.title = "Users List"

        usersListActivityViewModel = ViewModelProviders.of(this,appfactory).get(UsersListActivityViewModel::class.java)

        if (AppConstants.isNetworkConnected(this)) {
            usersListActivityViewModel.getUsersFromService().observe(this, Observer { it ->
                var list: List<UserEntity> = it
                Log.v("TAG","UserList from server ${list.size}")
                var mutableList: MutableList<UserEntity> = it as MutableList<UserEntity>
                usersListAdapter = UsersListAdapter(this, it)
                users_list_recyclerview.layoutManager = LinearLayoutManager(this)
                users_list_recyclerview.adapter = usersListAdapter

            })
        }else{

        usersListActivityViewModel.getUsers().observe(this, Observer { it ->

            var list: List<UserEntity> = it
            var mutableList: MutableList<UserEntity> = it as MutableList<UserEntity>

            usersListAdapter = UsersListAdapter(this, it)
            users_list_recyclerview.layoutManager = LinearLayoutManager(this)
            users_list_recyclerview.adapter = usersListAdapter
        })

        }
}
    override fun onItemClick(user: UserEntity) {
        Toast.makeText(this,"${user.userName}\n ${user.phoneNumber}", Toast.LENGTH_SHORT).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
