package com.acto.assignment.users.repository

import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.acto.assignment.restapi.ApiService
import com.acto.assignment.users.model.User
import com.acto.assignment.database.UserEntity
import com.acto.assignment.database.daos.UserEntityDao
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class UsersListRepository @Inject constructor(var retrofit: Retrofit,var userEntityDao: UserEntityDao)   {



    private val totalUsrsList = MutableLiveData<List<UserEntity>>()

    fun getUsers(): MutableLiveData<List<UserEntity>> {

        AsyncTask.execute {
            // Insert Data
            var userList : List<UserEntity> = userEntityDao.getUsersList()
            totalUsrsList.postValue(userList)

        }

        return totalUsrsList
    }


    // @Inject
    //  lateinit var entityDao: UserEntityDao

    fun insertUser(  user: UserEntity){

        someTask(userEntityDao,user).execute();

    }

    class someTask(var entityDao: UserEntityDao, var user: UserEntity) : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void?): String {
            entityDao.insertUser(user)

            return null.toString()
        }

    }

    fun getUsersFromService(): MutableLiveData<List<UserEntity>> {

        var userEntityList: List<UserEntity> = ArrayList<UserEntity>()

        var apiService  = retrofit.create(ApiService::class.java)
        apiService.getUsers().subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                var contactsResult = it
                Log.v("TAG","reseponse : $contactsResult")
                var json =      Gson().toJson(it)

                var list : List<User> = it
                list?.let {
                    for (item in it)
                    {
                        var userEntity : UserEntity = UserEntity(item.username,item.phone)
                        insertUser(userEntity)
                        (userEntityList!! as ArrayList).add(userEntity)

                    }
                    totalUsrsList.postValue(userEntityList)
                }


            },{

                Log.v("TAG","reseponse : $it")
            })

       return totalUsrsList
    }
    fun getUsersFromService1() {

        var apiService  = retrofit.create(ApiService::class.java)
        apiService.getUsers().subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                var contactsResult = it
                Log.v("TAG","reseponse : $contactsResult")
                var json =      Gson().toJson(it)

            },{

                Log.v("TAG","reseponse : $it")
            })

    }
}
