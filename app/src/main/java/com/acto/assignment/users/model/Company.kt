package com.acto.assignment.users.model

data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)