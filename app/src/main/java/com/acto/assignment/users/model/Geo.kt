package com.acto.assignment.users.model

data class Geo(
    val lat: String,
    val lng: String
)