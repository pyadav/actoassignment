package com.acto.assignment.users.viewmodele

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.acto.assignment.users.repository.UsersListRepository
import com.acto.assignment.database.UserEntity
import javax.inject.Inject

class UsersListActivityViewModel @Inject constructor(var usersListRepository: UsersListRepository) : ViewModel(){

    fun getUsers():MutableLiveData<List<UserEntity>>{

       var usersList =  usersListRepository.getUsers()

        return usersList

    }

    fun getUsersFromService(): MutableLiveData<List<UserEntity>>{
        var list : MutableLiveData<List<UserEntity>> = usersListRepository.getUsersFromService()
        Log.v("PRK","ViewModel")

       return list
    }

}