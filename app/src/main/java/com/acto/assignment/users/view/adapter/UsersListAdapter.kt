package com.acto.assignment.users.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.acto.assignment.R
import com.acto.assignment.databinding.UsersListItemBinding
import com.acto.assignment.database.UserEntity

class UsersListAdapter() : RecyclerView.Adapter<UsersListAdapter.DataHolder>() {

    lateinit var context: Context
    lateinit var itemClckListner: ItemClckListner
    lateinit var userList: MutableList<UserEntity>

    constructor(context: Context, userList: MutableList<UserEntity>) : this() {
        this.context = context
        this.userList = userList;
        itemClckListner = context as ItemClckListner

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
        var userListItem2Binding: UsersListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.users_list_item, parent, false
        )
        return DataHolder(userListItem2Binding, context, itemClckListner)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: DataHolder, position: Int) {

        var userEntity: UserEntity = userList.get(position)
        holder.itemBinding.userEntityData = userEntity
        holder.bind(userEntity)

    }

    class DataHolder(itemView: UsersListItemBinding, context: Context, itemClckListner: ItemClckListner) :
        RecyclerView.ViewHolder(itemView.root) {

        lateinit var itemClckListner: ItemClckListner

        var itemBinding = itemView

        init {
            this.itemClckListner = itemClckListner;
        }

        fun bind(user: UserEntity) {

            itemBinding.root.setOnClickListener {
                Log.v("TAG", " " + user.userName)
                itemClckListner.onItemClick(user)

            }
        }

    }

    public interface ItemClckListner {
        fun onItemClick(user: UserEntity)
    }

}