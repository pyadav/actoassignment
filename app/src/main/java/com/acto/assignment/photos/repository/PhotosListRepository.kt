package com.acto.assignment.users.repository

import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.acto.assignment.restapi.ApiService
import com.acto.assignment.database.daos.PhotoEntityDao
import com.acto.assignment.photos.model.Photo
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class PhotosListRepository @Inject constructor(var retrofit: Retrofit, var photoEntityDao : PhotoEntityDao)   {

    private val totalPhotosList = MutableLiveData<List<Photo>>()

    fun getPhotos(): MutableLiveData<List<Photo>> {

        AsyncTask.execute {
            // Insert Data
            var albumsList : List<Photo> = photoEntityDao.getPhotosList()
            totalPhotosList.postValue(albumsList)

        }

        return totalPhotosList
    }

    fun insertPhoto( photo: Photo){

        someTask(photoEntityDao,photo).execute();

    }

    class someTask(var entityDao: PhotoEntityDao, var entity: Photo) : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void?): String {
            entityDao.insertPhoto(entity)

            return null.toString()
        }

    }

    fun getPhotosFromService(): MutableLiveData<List<Photo>> {

        var photoEntityList: List<Photo> = ArrayList<Photo>()

        var apiService  = retrofit.create(ApiService::class.java)
        apiService.getPhotos().subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                var contactsResult = it
                Log.v("TAG","reseponse : $contactsResult")
                var json =      Gson().toJson(it)

                var list : List<Photo> = it
                list?.let {
                    for (item in it)
                    {
                       // var photoEntity : Photo = Photo(item.username,item.phone)
                        insertPhoto(item)
                       // (photoEntityList!! as ArrayList).add(Photo)

                    }
                    totalPhotosList.postValue(list)
                }


            },{

                Log.v("TAG","reseponse : $it")
            })

       return totalPhotosList
    }

}
