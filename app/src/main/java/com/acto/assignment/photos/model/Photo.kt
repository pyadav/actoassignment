package com.acto.assignment.photos.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Photo")
data class Photo(
    val albumId: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)
{
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}