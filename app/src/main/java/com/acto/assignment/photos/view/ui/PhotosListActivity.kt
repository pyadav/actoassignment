package com.acto.assignment.photos.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.acto.assignment.R
import com.acto.assignment.albums.view.adapter.AlbumsListAdapter
import androidx.lifecycle.Observer
import com.acto.assignment.albums.model.Album
import com.acto.assignment.databinding.ActivityAlbumsListBinding
import com.acto.assignment.albums.viewmodel.AlbumsListActivityViewModel
import com.acto.assignment.base.BaseActivity
import com.acto.assignment.photos.model.Photo
import com.acto.assignment.photos.view.adapter.PhotosListAdapter
import com.acto.assignment.users.viewmodele.PhotosListActivityViewModel
import com.acto.assignment.util.AppConstants
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_albums_list.*
import javax.inject.Inject

class PhotosListActivity : BaseActivity(), PhotosListAdapter.ItemClckListner {

    lateinit var binding : ActivityAlbumsListBinding
    lateinit var photosListActivityViewModel: PhotosListActivityViewModel
    lateinit var photosListAdapter: PhotosListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_albums_list)
        binding.lifecycleOwner = this;
        //set actionbar title
        var actionbar = getSupportActionBar()
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.title = "Photos"

        photosListActivityViewModel = ViewModelProviders.of(this,appfactory).get(PhotosListActivityViewModel::class.java)

        if (AppConstants.isNetworkConnected(this)) {
            photosListActivityViewModel.getPhotosFromService().observe(this, Observer { it ->
                var list: List<Photo> = it
                Log.v("TAG","UserList from server ${list.size}")
                var mutableList: MutableList<Photo> = it as MutableList<Photo>
                photosListAdapter = PhotosListAdapter(this, it)
                list_recyclerview.layoutManager = LinearLayoutManager(this)
                list_recyclerview.adapter = photosListAdapter

            })
        }else{

            photosListActivityViewModel.getPhotos().observe(this, Observer { it ->

           // var list: List<Photo> = it
            var mutableList: MutableList<Photo> = it as MutableList<Photo>

                photosListAdapter = PhotosListAdapter(this, it)
                list_recyclerview.layoutManager = LinearLayoutManager(this)
                list_recyclerview.adapter = photosListAdapter
        })

        }
}
    override fun onItemClick(photo: Photo) {
        Toast.makeText(this,"${photo.title}", Toast.LENGTH_SHORT).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
