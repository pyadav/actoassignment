package com.acto.assignment.users.viewmodele

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.acto.assignment.photos.model.Photo
import com.acto.assignment.users.repository.PhotosListRepository
import javax.inject.Inject

class PhotosListActivityViewModel @Inject constructor(var photosListRepository: PhotosListRepository) : ViewModel(){

    fun getPhotos():MutableLiveData<List<Photo>>{

       var photosList =  photosListRepository.getPhotos()

        return photosList

    }

    fun getPhotosFromService(): MutableLiveData<List<Photo>>{
        var list : MutableLiveData<List<Photo>> = photosListRepository.getPhotosFromService()
        Log.v("PRK","ViewModel")

       return list
    }

}