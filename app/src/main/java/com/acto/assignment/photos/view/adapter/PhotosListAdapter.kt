package com.acto.assignment.photos.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.acto.assignment.R
import com.acto.assignment.databinding.PhotosListItemBinding
import com.acto.assignment.photos.model.Photo
import com.acto.assignment.util.GlideApp

class PhotosListAdapter() : RecyclerView.Adapter<PhotosListAdapter.DataHolder>() {

    lateinit var context: Context
    lateinit var itemClckListner: ItemClckListner
    lateinit var photosList: MutableList<Photo>

    constructor(context: Context, photosList: MutableList<Photo>) : this() {
        this.context = context
        this.photosList = photosList;
        itemClckListner = context as ItemClckListner

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
        var photoListItemBinding : PhotosListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.photos_list_item, parent, false
        )
        photoListItemBinding.photoThumbnail


        return DataHolder(photoListItemBinding, context, itemClckListner)
    }

    override fun getItemCount(): Int {
        return photosList.size
    }

    override fun onBindViewHolder(holder: DataHolder, position: Int) {

        var photo: Photo = photosList.get(position)
        holder.itemBinding.photoEntity = photo
        Log.v("TAG","thumbnail"+photo.thumbnailUrl)

    }
    companion object {
        @JvmStatic
        @BindingAdapter(value = ["thumbnailUrl", "error"], requireAll = false)
        fun loadImage(view: ImageView, thumbnailUrl: String, error: Int) {

            GlideApp.with(view.context)
                .load(thumbnailUrl)
                .placeholder(R.mipmap.ic_launcher)
                .fitCenter()
                .into(view)
        }
    }


    class DataHolder(itemView: PhotosListItemBinding, context: Context, itemClckListner: ItemClckListner) :
        RecyclerView.ViewHolder(itemView.root) {


        lateinit var itemClckListner: ItemClckListner

        var itemBinding = itemView

        init {
            this.itemClckListner = itemClckListner;
        }

        fun bind(photo: Photo) {

            itemBinding.root.setOnClickListener {
                Log.v("TAG", " " + photo.title)
                itemClckListner.onItemClick(photo)

            }

           /* Glide.with(context)
                .load(photo.thumbnailUrl)
                .into(itemBinding.photoThumbnail);*/

        }

    }

    public interface ItemClckListner {
        fun onItemClick(photo: Photo)
    }

}