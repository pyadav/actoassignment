package com.acto.assignment.albums.repository

import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.acto.assignment.albums.model.Album
import com.acto.assignment.restapi.ApiService
import com.acto.assignment.database.daos.AlbumEntityDao
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import javax.inject.Inject

class AlbumListRepository @Inject constructor(var retrofit: Retrofit,var albumEntityDao : AlbumEntityDao)   {

    private val totalAlbumsList = MutableLiveData<List<Album>>()

    fun getAlbums(): MutableLiveData<List<Album>> {

        AsyncTask.execute {
            // Insert Data
            var albumsList : List<Album> = albumEntityDao.getAlbumsList()
            totalAlbumsList.postValue(albumsList)

        }

        return totalAlbumsList
    }

    fun insertAlbum( album: Album){

        someTask(albumEntityDao,album).execute();

    }

    class someTask(var entityDao: AlbumEntityDao, var entity: Album) : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void?): String {
            entityDao.insertAlbum(entity)

            return null.toString()
        }

    }

    fun getAlbumsFromService(): MutableLiveData<List<Album>> {

        var albumEntityList: List<Album> = ArrayList<Album>()

        var apiService  = retrofit.create(ApiService::class.java)
        apiService.getAlbums().subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                var contactsResult = it
                Log.v("TAG","reseponse : $contactsResult")
                var json =      Gson().toJson(it)

                var list : List<Album> = it
                list?.let {
                    for (item in it)
                    {
                       // var albumEntity : Album = Album(item.username,item.phone)
                        insertAlbum(item)
                       // (albumEntityList!! as ArrayList).add(Album)

                    }
                    totalAlbumsList.postValue(list)
                }


            },{

                Log.v("TAG","reseponse : $it")
            })

       return totalAlbumsList
    }

}
