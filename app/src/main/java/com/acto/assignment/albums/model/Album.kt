package com.acto.assignment.albums.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Album")
data class Album(
    val title: String,
    val userId: Int
)
{
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

}