package com.acto.assignment.albums.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.acto.assignment.R
import com.acto.assignment.albums.model.Album
import com.acto.assignment.databinding.AlbumsListItemBinding

class AlbumsListAdapter() : RecyclerView.Adapter<AlbumsListAdapter.DataHolder>() {

    lateinit var context: Context
    lateinit var itemClckListner: ItemClckListner
    lateinit var albumsList: MutableList<Album>

    constructor(context: Context, albumList: MutableList<Album>) : this() {
        this.context = context
        this.albumsList = albumList;
        itemClckListner = context as ItemClckListner

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
        var albumListItemBinding :AlbumsListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.albums_list_item, parent, false
        )
        return DataHolder(albumListItemBinding, context, itemClckListner)
    }

    override fun getItemCount(): Int {
        return albumsList.size
    }

    override fun onBindViewHolder(holder: DataHolder, position: Int) {

        var album: Album = albumsList.get(position)
        holder.itemBinding.albumEntity = album
        holder.bind(album)

    }

    class DataHolder(itemView: AlbumsListItemBinding, context: Context, itemClckListner: ItemClckListner) :
        RecyclerView.ViewHolder(itemView.root) {

        lateinit var itemClckListner: ItemClckListner

        var itemBinding = itemView

        init {
            this.itemClckListner = itemClckListner;
        }

        fun bind(album: Album) {

            itemBinding.root.setOnClickListener {
                Log.v("TAG", " " + album.title)
                itemClckListner.onItemClick(album)

            }
        }

    }

    public interface ItemClckListner {
        fun onItemClick(album: Album)
    }

}