package com.acto.assignment.albums.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.acto.assignment.R
import com.acto.assignment.albums.view.adapter.AlbumsListAdapter
import androidx.lifecycle.Observer
import com.acto.assignment.albums.model.Album
import com.acto.assignment.databinding.ActivityAlbumsListBinding
import com.acto.assignment.albums.viewmodel.AlbumsListActivityViewModel
import com.acto.assignment.base.BaseActivity
import com.acto.assignment.util.AppConstants
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_albums_list.*
import javax.inject.Inject

class AlbumsListActivity : BaseActivity(), AlbumsListAdapter.ItemClckListner {

    lateinit var binding : ActivityAlbumsListBinding
    lateinit var albumsListAdapter: AlbumsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_albums_list)
        binding.lifecycleOwner = this;
        //set actionbar title
        var actionbar = getSupportActionBar()
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.title = "Albums"

        albusListActivityViewModel = ViewModelProviders.of(this,appfactory).get(AlbumsListActivityViewModel::class.java)

        if (AppConstants.isNetworkConnected(this)) {
            albusListActivityViewModel.getAlbumsFromService().observe(this, Observer { it ->
                var list: List<Album> = it
                Log.v("TAG","UserList from server ${list.size}")
                var mutableList: MutableList<Album> = it as MutableList<Album>
                albumsListAdapter = AlbumsListAdapter(this, it)
                list_recyclerview.layoutManager = LinearLayoutManager(this)
                list_recyclerview.adapter = albumsListAdapter

            })
        }else{

            albusListActivityViewModel.getAlbums().observe(this, Observer { it ->

            var list: List<Album> = it
            var mutableList: MutableList<Album> = it as MutableList<Album>

                albumsListAdapter = AlbumsListAdapter(this, it)
                list_recyclerview.layoutManager = LinearLayoutManager(this)
                list_recyclerview.adapter = albumsListAdapter
        })

        }
}
    override fun onItemClick(album: Album) {
        Toast.makeText(this,"${album.title}", Toast.LENGTH_SHORT).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
