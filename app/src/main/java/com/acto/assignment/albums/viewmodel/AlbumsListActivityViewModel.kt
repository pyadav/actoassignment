package com.acto.assignment.albums.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.acto.assignment.albums.model.Album
import com.acto.assignment.albums.repository.AlbumListRepository
import javax.inject.Inject

class AlbumsListActivityViewModel @Inject constructor(var albumListRepository: AlbumListRepository) : ViewModel(){

    fun getAlbums():MutableLiveData<List<Album>>{

       var albumsList =  albumListRepository.getAlbums()

        return albumsList

    }

    fun getAlbumsFromService(): MutableLiveData<List<Album>>{
        var list : MutableLiveData<List<Album>> = albumListRepository.getAlbumsFromService()
        Log.v("PRK","ViewModel")

       return list
    }

}