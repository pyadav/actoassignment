package com.acto.assignment.util

import android.Manifest
import android.content.Context
import android.net.ConnectivityManager

class AppConstants {
    companion object {

        var BASE_URL: String = "https://jsonplaceholder.typicode.com/"


        var CAMERA_PERMISSION: String = Manifest.permission.CAMERA
        var READ_EXTERNAL_STORAGE_PERMISSION: String = Manifest.permission.READ_EXTERNAL_STORAGE
        var WRITE_EXTERNAL_STORAGE_PERMISSION: String = Manifest.permission.WRITE_EXTERNAL_STORAGE

        val CAMERA_SDCARD_REQUEST =
            listOf<String>(CAMERA_PERMISSION, READ_EXTERNAL_STORAGE_PERMISSION, WRITE_EXTERNAL_STORAGE_PERMISSION)


        fun isNetworkConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            return cm!!.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected
        }


    }
}