package com.acto.assignment;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import com.acto.assignment.di.AppInject;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasFragmentInjector;

import javax.inject.Inject;

public class AppApplication extends Application implements HasActivityInjector, HasFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;


    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }


    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }



    @Override
    public void onCreate() {
        super.onCreate();

        /*   DaggerMyAppComponent.builder()
                .application(this)
                .build()
                .inject(this);*/
        AppInject.init(this);

    }

}